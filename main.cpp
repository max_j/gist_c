#include <iostream>
#include <cpr/cpr.h>

using namespace std;

void print_all_gists(string token){
    cpr::Response r = cpr::Get(cpr::Url{"https://api.github.com/gists"}, cpr::VerifySsl(0),
                               cpr::Bearer{token});



    if (r.status_code == 200) {
        cout << "Udalo sie wyswietlic liste" << endl;
    }
    else {
        cout << "Nie udalo sie wyswietlic listy" << endl;
    }

    cout << r.status_code << endl;
    cout << r.text << endl;
}

void print_content_specific_gist(string token, string gist_id) {
//    string url = "https://api.github.com/gists/{" + gist_id + "}";
    string url = "https://api.github.com/gists/" + gist_id;
    cpr::Response r = cpr::Get(cpr::Url{url}, cpr::VerifySsl(0),
                               cpr::Bearer{token});


    if (r.status_code == 200) {
        cout << "Udalo sie wyswietlic zawartosc gistu" << endl;
    }
    else {
        cout << "Nie udalo sie wyswietlic zawartosci gistu" << endl;
    }

    cout << r.status_code << endl;
    cout << r.text << endl;
}

void remove_gist(string token, string gist_id) {
//    string url = "https://api.github.com/gists/{" + gist_id + "}";
    string url = "https://api.github.com/gists/" + gist_id;
    cpr::Response r = cpr::Delete(cpr::Url{url}, cpr::VerifySsl(0),
                               cpr::Bearer{token});

    if (r.status_code == 204) {
        cout << "Udalo sie usunac gist" << endl;
    }
    else {
        cout << "Nie udalo sie usunac gista" << endl;
    }

    cout << r.text << endl;
}


void add_gist(string token, string title, string description){
    cpr::Response r = cpr::Post(cpr::Url{"https://api.github.com/gists"}, cpr::VerifySsl(0),
                                cpr::Parameters{{"description", description}},
                                cpr::Multipart{
                                {"gist1.py", cpr::File{"gist1.py"}}},
                                cpr::Bearer{token});

    if (r.status_code == 201) {
        cout << "Udalo sie stworzyc nowy gist" << endl;
    }
    else {
        cout << "Nie udalo sie stworzyc gista" << endl;
    }

    cout << r.text << endl;
}

int main(int argc, char** argv) {

    string token;
    char option;

    int end = 0;

    cout << "Prosze podac token uwiezytelniajacy: " << endl;
    cin >> token;

    while (end != 1) {


    //roboczy token z konta stworzonego na potrzeby tej aplikacji
//        token = "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";

        cout << "Co chcesz zrobic?" << endl;
        cout << "1. Wyswietl liste wszyskich gistow." << endl;
        cout << "2. Wyswietlic konkretny gist." << endl;
        cout << "3. Dodac gist." << endl;
        cout << "4. Usun gist." << endl;
        cout << "5. Koniec programu." << endl;

        cin >> option;

        if (option == '5') {
            end = 1;
            break;
        }

        else if (option == '1') {
            print_all_gists(token);
        }

        else if (option == '2') {
            string gist_id;
            cout << "Prosze podac id gistu:" << endl;
            cin >> gist_id;
            print_content_specific_gist(token, gist_id);
        }

        else if (option == '3') {
            string title, description;

            cout << "Prosze tutul nowego gistu:" << endl;
            cin >> title;
            cout << "Prosze opis nowego gistu:" << endl;
            cin >> description;
            add_gist(token, title, description);
        }

        else if (option == '4') {
            string title, description;
            string gist_id;
            cout << "Prosze podac id gistu do usuniecia:" << endl;
            cin >> gist_id;
            remove_gist(token, gist_id);
        }

    }

    cout << "Koniec programu." << endl;

}